import numpy as np
import scipy.misc
#import scipy.stats
import random
import matplotlib.pyplot as plt
from timeit import default_timer as timer
from mpl_toolkits.mplot3d import Axes3D


# Calculates the probability of stating a target number, then
# in num_rolls number of rolls getting num_dice to all show up
# as the target number
# Note to self, can greatly increase efficiency of this program
# using dynammic programming instead of strictly recursive approach here
# should reduce complexity from exponential to quadratic
def shake_Of_The_Day(num_dice, num_rolls):
	if num_dice <= 0:
		return 1
	elif num_rolls == 1:
		return binomial(num_dice, num_dice, 1/6)

	currentProb = 0
	for i in range(num_dice + 1):
		currentProb += binomial(num_dice, i, 1/6) * shake_Of_The_Day(num_dice - i, num_rolls - 1)

	return currentProb

def dynammic_shake(num_dice, num_rolls):
	probs = np.empty((num_rolls + 1, num_dice + 1))
	for i in range(num_rolls + 1):
		for j in range(num_dice + 1):
			if j == 0:
				probs[i][j] = 1
				continue
			elif j >= 1 and i == 0:
				probs[i][j] = 0
			elif i == 1 and j >= 1:
				probs[i][j] = binomial(j, j, 1/6)
			else:
				probs[i][j] = sum([probs[i-1][k] * binomial(j, j - k, 1/6) for k in range(0, j + 1)])
	return probs
# Simple binomial calculator. Returns the probability of k successes
# in n trials if the probability of success each trial is p.
def binomial(n, k, p):
	num_ways = scipy.misc.comb(n, k)
	return num_ways * (p**k) * ((1-p)**(n-k))

# Sanity check for correctness of probability given by shake_Of_The_Day.
# Runs a simulation num_trials number of times and reports the fraction
# of successes. By law of large numbers this fraction should get arbitrarily
# close to 0 as num_trials increases.
def prob_sim(num_dice, num_trials, num_rolls):
    got = 0
    if num_dice == 0:
        return 1
    for num in range(num_trials):
        left_to_get = num_dice
        target = random.randint(1, 6)
        for j in range(num_rolls):
            for i in range(left_to_get):
                hit = random.randint(1, 6)
                if hit == target:
                    left_to_get -= 1
        if left_to_get == 0:
            got += 1
    return (got/num_trials)


# Plots shake_Of_The_Day in 3d with respect to the number of dice
# and the number of rolls.
def plot(dice_end, rolls_end):
	x = np.arange(start=0, stop=rolls_end + 1)
	y = np.arange(start=0, stop=dice_end + 1)

	Z = dynammic_shake(dice_end, rolls_end)

	hf = plt.figure()
	ha = hf.add_subplot(111, projection='3d')

	X, Y = np.meshgrid(x, y)  # `plot_surface` expects `x` and `y` data to be 2D
	ha.plot_surface(X, Y, Z)

	ha.set_xlabel('# of Dice')
	ha.set_ylabel('# of Rolls')
	ha.set_zlabel('Probability of Success')
	
	ha.view_init(30, 60)
	plt.show()

def plot_num_dice(start, stop, num_rolls):
	x = np.arange(start=start, stop=stop)
	y = []

	for num in x:
		y.append(shake_Of_The_Day(num, num_rolls))

	plt.plot(x, y)


	plt.show()

def plot_num_rolls(start, stop, num_dice):
	x = np.arange(start=start, stop=stop)
	y = []

	for num in x:
		y.append(shake_Of_The_Day(num_dice, num))

	plt.plot(x, y)
	plt.xlabel('Numer of rolls')
	plt.ylabel('Probability of success')

	plt.show()

dynammic_shake(20, 20)